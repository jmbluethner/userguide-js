/**
 * @author bluethner <bluethner@heliophobix.com>
 * @license MIT
 * @description This project allows you to guide users through your application
 */

/**
 * @param dataGuideValue string
 * @return {boolean|(number|string|Window)[]}
 */
function getElementCoordinatesAndSizeByDataGuide(dataGuideValue) {
     let el = document.querySelectorAll('[data-guide='+dataGuideValue+']');
     if(el === null || el === undefined ||el.length === 0) {
         // Couldn't find an element with the given data-value
         return false;
     } else if(el.length > 1) {
         // Found more than one element with the given data-value
         return false;
     }
     let rect = el.getBoundingClientRect();
     let x = rect.left;
     let y = rect.top;
     let h = rect.bottom - rect.top;
     let b = rect.right - rect.left;
     return [x,y,h,b];
}

/**
 * @description create the guide overlay
 */
function createGuideOverlay() {
    // Create the container element
    let overlay = document.createElement('div');
    overlay.setAttribute('id','userguide_overlay');
    // Create the text node
    let textbox = document.createElement('div');
    textbox.setAttribute('id','userguide_overlay_textbox');

    // Add the text node to the container
    overlay.appendChild('textbox');
    // Add the container to the document
    document.body.appendChild(overlay);
}

/**
 * @description Cut out a shape in the overlay div to make the
 * @param clippingObj - The Coordinates and size of the object that should be visible through the clip. Get it by using getElementCoordinatesAndSizeByDataGuide()
 */
function setClippingPathToOverlay(clippingObj) {
    let overlay = document.getElementById('userguide_overlay');
    overlay.style.clipPath = 'inset('+clippingObj[0]+','+clippingObj[1]+','+clippingObj[2]+','+clippingObj[3]+')';
}

/**
 * @description Delete the clipping path on the overlay div
 */
function removeOverlayClipPath() {
    let overlay = document.getElementById('userguide_overlay');
    overlay.style.clipPath = 'none';
}

/**
 *
 * @param text
 * @return {boolean}
 */
function setOverlayText(text) {
    let textbox = document.getElementById('userguide_overlay_textbox');
    textbox.style.innerHTML = text;
    // Check
    return textbox.style.innerHTML === text;

}

/**
 * @description Deletes the overlay div
 * @return {boolean}
 */
function removeGuideOverlay() {
    let el = document.getElementById('userguide_overlay');
    el.remove();
    // Check if the deletion was successful
    return document.getElementById('userguide_overlay') === null;

}

function showNextGuide() {

}

/**
 * @description The main guide object
 */
class Guide {
    /**
     * @param steps 2D Array [['text','object'],['text','object']]
     */
    constructor(steps) {
        this.steps = steps;
        this.currentStep = 0;
    }
    create() {
        createGuideOverlay();
    }
    showNextGuide() {
        if(this.currentStep > this.steps.length) {
            // Array end reached
            return false;
        } else {
            // Show next question
            let step = this.steps[this.currentStep]
            setOverlayText(step[0]);
            setClippingPathToOverlay(getElementCoordinatesAndSizeByDataGuide(step[1]));
            this.currentStep++;
        }
    }
    delete() {
        removeGuideOverlay();
    }
}